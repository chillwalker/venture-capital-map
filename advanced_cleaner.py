# Import libraries
import numpy as np
import pandas as pd
import re
import geocoder
import time


# Import data
df = pd.read_csv("strictlyvc_tmp.csv", encoding='utf-8')


##############################################################################


# Cleaning age
age = df['age']

# Years numerical
repl = lambda m: str(int(float(m.group(1)) * 12))
age = age.astype(str).str.replace(r"^,?\s*\S*\s+(?:nearly\s?|now\s)?(\d+\.?\d?)-year", repl)

# Years textual
repl = lambda m: "!" + m.group(1) # With the leading "!" we don't mess up the "three-months" etc.
age = age.str.replace(r"^,?\s*\S*\s+(?:nearly-?\s?|now\s)?([a-z]*)-year", repl)
tmp = {'!one': '12',
       '!two': '24',
       '!three': '36',
       '!four': '48',
       '!five': '60',
       '!six': '72',
       '!seven': '84',
       '!eight': '96',
       '!nine': '108',
       '!ten': '120',
       '!eleven': '132',
       '!twelve': '144'}
for key, value in tmp.items():
    age = age.str.replace(key, value)

# Months numerical
repl = lambda m: m.group(1)
age = age.str.replace("^,?\s*\S*\s+(?:nearly-?\s?|now\s)?(\d+)-months?", repl)

# Months textual
repl = lambda m: "%" + m.group(1)
age = age.str.replace("^,?\s*\S*\s+(?:nearly-?\s?|now\s)?([a-z]*)-months?", repl)
tmp = {'%one': '1',
       '%two': '2',
       '%three': '3',
       '%four': '4',
       '%five': '5',
       '%six': '6',
       '%seven': '7',
       '%eight': '8',
       '%nine': '9',
       '%ten': '10',
       '%eleven': '11',
       '%twelve': '12'}
for key, value in tmp.items():
    age = age.str.replace(key, value)

# Special cases
# Todo: fix those items with <span style="..."></span> sometime
age = age.str.replace(", a nearly year", "11")
age = age.str.replace(",?\s?a? year", "12")
age = age.str.replace(", the decade", "120")
age = age.str.replace(", a months", "3")

# Replace everything not fixed as nan
age = pd.to_numeric(age, errors = "coerce")

# Append to dataframe
#df.insert(2, "age_clean", age)
df['age'] = age


##############################################################################


# Clean location
location = df['location']

# Remove "dunno"
location = location.str.replace(", dunno", "")

# Remove leading ", "
location = location.str.replace("^[,.]?\s", "")

# If there is more than one city -> reduce to first city and state
location = location.str.replace("(.*)(- and.*)", "\\1")

# Remove <span>-tags
location = location.str.replace("<.*>(.*)", "\\1")

# Earlier queries
# Get city name: ^(?:[,.])?\s?(\w+\s?\w+\s?\w+)

# Append to dataframe
#df.insert(4, "location_clean", location)
df['location'] = location


##############################################################################


# Clean description
description = df['description']

description = description.str.replace("<[^<]+?>", "")
description = description.str.replace("&#39;", "'")
description = description.str.replace("\u200b", "")
#df.insert(6, "description_clean", description)
df['description'] = description


##############################################################################


# Clean amount
amount = df['amount']

# Fix broken html markup
amount = amount.str.strip()
amount = amount.str.replace("<[^<]+?>", "")
amount = amount.str.replace("<span.*", "")

# Todo: Add Yen ¥
amount = amount.str.replace("&pound;", "£")
amount = amount.str.replace("&euro;", "€")

# Get the amount as float in million
number = amount.str.findall(r"(\d+[\.|,]?\d*)")
for index, value in number.iteritems():
    # findall gives back an object that contains either lists with content, empty lists or nan which is of type float
    # print("Index: {} | Value: {} | type(value) = {}".format(index, value, type(value)))
    if isinstance(value, list) and len(value):
        if "," in value[0]: # Handle values like 650,000
            number[index] = float(value[0].replace(',','')) / 1000000
        else:
            number[index] = float(value[0])
    else:
        number[index] = np.nan

# Get the plain currency symbol
currency = amount.str.findall(r"[$|€|£]")
for index, value in currency.iteritems():
    if isinstance(value, list):
        if len(value):
            currency[index] = value[0]
    else:
        currency[index] = np.nan

# TODO: Get rates dynamically
rates = {"pound-usd": 1.33,
         "euro-usd": 1.11}

# Change all currencies to US dollar
amount_mio_usdollar = []
for index, value in currency.iteritems():
    # print("Value: {} | Index: {}".format(value, index))
    if value == '$':
        amount_mio_usdollar.append(number[index])
    elif value == '€':
        amount_mio_usdollar.append(round(number[index] * rates['euro-usd'], 2))
    elif value == '£':
        amount_mio_usdollar.append(round(number[index] * rates['pound-usd'], 2))
    else:
        amount_mio_usdollar.append(np.nan)

df.insert(5, "amount_mio_usd", amount_mio_usdollar)

##############################################################################


# Get latitude, longitude, city and country from location

# Reset/init values
'''
df['latitude'] = np.nan
df['longitude'] = np.nan
df['city'] = ""
df['state'] = ""
df['country'] = ""
'''
parts = {
    "latitude": "y",
    "longitude": "x",
    "city": "addr:city",
    "state": "addr:state",
    "country": "addr:country"
}
reqCount = 0
for index, row in df.iterrows():

    # Skip where we already have a valid value
    if not pd.isnull(df.at[index, 'latitude']):
        print("skipping...")
        continue

    # Wait every 100 requests for 2 minutes to prevent getting blocked
    if reqCount == 10:
        print("######## Saving progress and waiting for 15 seconds...")
        df.to_csv("strictlyvc_tmp.csv", index = False)
        time.sleep(15)
        reqCount = 0

    # Get the data and update dataframe
    location = row['location']
    g = geocoder.osm(location)
    time.sleep(10)  # Wait 10 seconds between requests to prevent getting blocked
    reqCount = reqCount + 1
    print("#### Waited 10 seconds... reqCount = %d | index is: %d" % (reqCount, index))

    if g.osm:
        print("## We have an osm answer")
        for key, value in parts.items():
            print("key: %s | value: %s | g.osm: %s" % (key, value, g.osm.get(value)))
            df.at[index, key] = g.osm.get(value)
    else:
        print("## No osm")
        for key, value in parts.items():
            df.at[index, key] = np.nan


##############################################################################


# Update input file
df.to_csv("strictlyvc_tmp.csv", index=False)
